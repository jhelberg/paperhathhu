#!/usr/bin/python
from radiostuff import radioControl
from time import sleep

print( "testing radio stuff" )

class radiovalues( radioControl ):
    def changedVolume( self ):
        print( "Volume  changed: ", self.getvolume() )
    def changedStation( self ):
        print( "Station changed: ", self.stationName( self.getservice() ) )

rv = radiovalues()
rv.changedVolume()   # first time here, assume Volume  changed
rv.changedStation()  # first time here, assume Station changed
rv.startPolling()

while True:
    sleep( 10 )
