#!/usr/bin/python
import epd2in13b
import epd2in13
import time
import Image
import ImageDraw
import ImageFont
import ImageChops
from radiostuff import radioControl

theDisplay='B'

if theDisplay == 'A':
    epd = epd2in13.EPD()
    epd.init( epd.lut_full_update )
    epd.clear_frame_memory(0xFF)
    epd.display_frame()
    epd.delay_ms(2000)
    epd.clear_frame_memory(0xFF)
    epd.display_frame()
    epd.init(epd.lut_partial_update)
    epd.clear_frame_memory(0xFF)
    epd.display_frame()
    epd.clear_frame_memory(0xFF)
    epd.display_frame()
    epd.delay_ms(2000)
    width  = epd2in13.EPD_HEIGHT
    height = epd2in13.EPD_WIDTH - 4
if theDisplay == 'B':
    epd = epd2in13b.EPD()
    epd.init()
    epd.Clear()
    width  = epd2in13b.EPD_HEIGHT
    height = epd2in13b.EPD_WIDTH

stationFont      = ImageFont.truetype( "/usr/share/fonts/truetype/freefont/FreeSansBoldOblique.ttf", 
                                       int(height * 0.18) )
stationFontSmall = ImageFont.truetype( "/usr/share/fonts/truetype/freefont/FreeSansBoldOblique.ttf", 
                                       int(height * 0.14) )
volumeFont       = ImageFont.truetype( "/usr/share/fonts/truetype/freefont/FreeSansBold.ttf",
                                       int(height * 0.38) )
logoFont         = ImageFont.truetype( "/usr/share/fonts/truetype/freefont/FreeSansBold.ttf",
                                       int(height * 0.14) - 3 )

LogoImage = Image.new('1', (width, height), 255)
LogoDraw = ImageDraw.Draw( LogoImage )
LogoDraw.text( (int(int( width * 0.04)/3), height - 14), "jmm case prods", font = logoFont, fill = 0 )
if theDisplay == 'B':
    epd.displayRed(epd.getbuffer(LogoImage))

class radiovalues( radioControl ):
    current_volume  = 0
    current_station = 0
    def changedVolume( self ):
        print( "Draw new Volume : ", self.getvolume(), 
               self.stationName( self.getservice() ) )
        if self.current_volume == self.getvolume():
            return
        self.current_volume  = self.getvolume()
        self.current_station = self.getservice()
        sname = self.stationName( self.current_station )
        vnumb = str( self.current_volume )
        if theDisplay == 'A':
            width  = epd2in13.EPD_HEIGHT
            height = epd2in13.EPD_WIDTH - 4
        if theDisplay == 'B':
            width  = epd2in13b.EPD_HEIGHT
            height = epd2in13b.EPD_WIDTH
        StationImage = Image.new('1', (width, height), 255)
        Stationdraw = ImageDraw.Draw( StationImage )
        if Stationdraw.textsize( sname, font = stationFont )[0] < int( width - width * 0.05):
           Stationdraw.text( (int( width * 0.04), int( height * 0.07)), sname, font = stationFont, fill = 0 )
        else:
           words = sname.split(' ')
           half = int( round( len( words ) / 2 ) )
           s = " "
           sname = s.join( words[ :half ] ) + "\n " + s.join( words[ half: ] )
           if Stationdraw.textsize( sname, font = stationFont )[0] < int( width - width * 0.05):
               Stationdraw.text( (int( width * 0.04), int( height * 0.07)), sname, font = stationFont, fill = 0 )
           else:
               Stationdraw.text( (int( width * 0.04), int( height * 0.07)), sname, font = stationFontSmall, fill = 0 )
        
        VolumeImage = Image.new('1', (width, height), 255)
        Volumedraw = ImageDraw.Draw( VolumeImage )
        voltop = int( height - height * 0.07) - Volumedraw.textsize( vnumb, font = volumeFont)[1]
        volleft = int( width - width * 0.05) - Volumedraw.textsize( vnumb, font = volumeFont)[0]
        Volumedraw.text( (volleft, voltop), vnumb, font = volumeFont, fill = 0 )
        if theDisplay == 'A':
            result = ImageChops.logical_xor( StationImage, VolumeImage )
            result = ImageChops.logical_xor( result, LogoImage )
            #result = result.Crop( ( voltop[0], voltop[1], volleft[0], volleft[1] ) )
            result = result.rotate( 90, expand = 1 )
            epd.set_frame_memory( result, 0, 0 )
            epd.display_frame()
        if theDisplay == 'B':
            result = ImageChops.logical_xor( StationImage, VolumeImage )
            result = ImageChops.invert( result )
            epd.displayBlack( epd.getbuffer( result ) )
        
        StationImage.close()
        VolumeImage.close()

    def changedStation( self ):
        print( "Draw new Station: ", self.getvolume(), 
               self.stationName( self.getservice() ) )
        if self.current_station == self.getservice():
            return
        self.current_volume  = self.getvolume()
        self.current_station = self.getservice()
        sname = self.stationName( self.current_station )
        vnumb = str( self.current_volume )
        if theDisplay == 'A':
            width  = epd2in13.EPD_HEIGHT
            height = epd2in13.EPD_WIDTH - 4
        if theDisplay == 'B':
            width  = epd2in13b.EPD_HEIGHT
            height = epd2in13b.EPD_WIDTH
        StationImage = Image.new('1', (width, height), 255)
        Stationdraw = ImageDraw.Draw( StationImage )
        if Stationdraw.textsize( sname, font = stationFont )[0] < int( width - width * 0.05):
           Stationdraw.text( (int( width * 0.04), int( height * 0.07)), sname, font = stationFont, fill = 0 )
        else:
           words = sname.split(' ')
           half = int( round( len( words ) / 2 ) )
           s = " "
           sname = s.join( words[ :half ] ) + "\n " + s.join( words[ half: ] )
           if Stationdraw.textsize( sname, font = stationFont )[0] < int( width - width * 0.05):
               Stationdraw.text( (int( width * 0.04), int( height * 0.07)), sname, font = stationFont, fill = 0 )
           else:
               Stationdraw.text( (int( width * 0.04), int( height * 0.07)), sname, font = stationFontSmall, fill = 0 )
        
        VolumeImage = Image.new('1', (width, height), 255)
        Volumedraw = ImageDraw.Draw( VolumeImage )
        voltop = int( height - height * 0.07) - Volumedraw.textsize( vnumb, font = volumeFont)[1]
        volleft = int( width - width * 0.05) - Volumedraw.textsize( vnumb, font = volumeFont)[0]
        Volumedraw.text( (volleft, voltop), vnumb, font = volumeFont, fill = 0 )
        if theDisplay == 'A':
            epd.reset()
            result = ImageChops.logical_xor( StationImage, VolumeImage )
            result = ImageChops.logical_xor( result, LogoImage )
            #result = result.Crop( ( int( width * 0.04), int( height * 0.07), int( width - width * 0.05), int( height * 0.07) + int(height * 0.18) * 2 ) )
            result = result.rotate( 90, expand = 1 )
            epd.set_frame_memory( result, 0, 0 )
            epd.display_frame()
            epd.sleep()
        if theDisplay == 'B':
            result = ImageChops.logical_xor( StationImage, VolumeImage )
            result = ImageChops.invert( result )
            epd.displayBlack( epd.getbuffer( result ) )
        
        StationImage.close()
        VolumeImage.close()

rv = radiovalues()
rv.changedVolume()   # first time here, assume Volume  changed
rv.changedStation()  # first time here, assume Station changed
rv.startPolling()

while True:
    time.sleep( 10 )

LogoImage.close()
