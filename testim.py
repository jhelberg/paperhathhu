#!/usr/bin/python
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from PIL import ImageChops

theDisplay = 'A'
if theDisplay == 'A':
    width  = 212
    height = 104
if theDisplay == 'B':
    width  = 250
    height = 128

stationFont      = ImageFont.truetype( "/usr/share/fonts/truetype/freefont/FreeSansBoldOblique.ttf", 
                                       int(height * 0.18) )
stationFontSmall = ImageFont.truetype( "/usr/share/fonts/truetype/freefont/FreeSansBoldOblique.ttf", 
                                       int(height * 0.14) )
volumeFont       = ImageFont.truetype( "/usr/share/fonts/truetype/freefont/FreeSansBold.ttf", 
                                       int(height * 0.38) )
logoFont         = ImageFont.truetype( "/usr/share/fonts/truetype/freefont/FreeSansBold.ttf", 
                                       int(height * 0.14) - 3 )

sname = "BBC Radio 5 Live"
vnumb = str( 56 )
StationImage = Image.new('1', (width, height), 255)
Stationdraw = ImageDraw.Draw( StationImage )
if Stationdraw.textsize( sname, font = stationFont )[0] < int( width - width * 0.05):
   Stationdraw.text( (int( width * 0.04), int( height * 0.07)), sname, font = stationFont, fill = 0 )
else:
   words = sname.split(' ')
   half = int( round( len( words ) / 2 ) )
   s = " "
   sname = s.join( words[ :half ] ) + "\n " + s.join( words[ half: ] )
   if Stationdraw.textsize( sname, font = stationFont )[0] < int( width - width * 0.05):
       Stationdraw.text( (int( width * 0.04), int( height * 0.07)), sname, font = stationFont, fill = 0 )
   else:
       Stationdraw.text( (int( width * 0.04), int( height * 0.07)), sname, font = stationFontSmall, fill = 0 )

VolumeImage = Image.new('1', (width, height), 255)
Volumedraw = ImageDraw.Draw( VolumeImage )
voltop = int( height - height * 0.07) - Volumedraw.textsize( vnumb, font = volumeFont)[1]
volleft = int( width - width * 0.05) - Volumedraw.textsize( vnumb, font = volumeFont)[0]
Volumedraw.text( (volleft, voltop), vnumb, font = volumeFont, fill = 0 )
print( "voltop: ", voltop )
print( "volleft: ", volleft )
LogoImage = Image.new('1', (width, height), 255)
LogoDraw = ImageDraw.Draw( LogoImage )
LogoDraw.text( (int(int( width * 0.04)/3), height - 14), "jmm case prods", font = logoFont, fill = 0 )
result = ImageChops.logical_xor( StationImage, VolumeImage )
result = ImageChops.invert( result )
result = ImageChops.logical_xor( LogoImage, result )
result = ImageChops.invert( result )
if theDisplay == 'A':
    result = result.rotate(90, expand = 1)
LogoImage.close()
StationImage.close()
VolumeImage.close()
result.save( "epap.jpg", "JPEG")
result.show()
