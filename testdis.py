#!/usr/bin/python
import epd2in13b
import epd2in13
import time
import Image
import ImageDraw
import ImageFont
import ImageChops
from radiostuff import radioControl

theDisplay='B'

if theDisplay == 'A':    
    epd = epd2in13.EPD()
    print( "epd present" )
    epd.init( epd.lut_full_update )
    print( "init done" )
    epd.clear_frame_memory(0xFF)
    epd.display_frame()
    print( "1st display done" )
    epd.clear_frame_memory(0xFF)
    epd.display_frame()
    print( "2nd display done" )
    epd.init( epd.lut_partial_update )
    image = Image.new( '1', (16, 10), 255 )
    for x in range( 0, epd2in13.EPD_WIDTH, 16 ):
        for y in range( 0, epd2in13.EPD_HEIGHT, 10 ):
            epd.set_frame_memory( image, x, y )
            epd.display_frame()
            epd.set_frame_memory( image, x, y )
            epd.display_frame()
            print( x, y )
    image.close()
    image = Image.new( '1', (32, 25), 255 )
    for x in range( 0, epd2in13.EPD_WIDTH, 32 ):
        for y in range( 0, epd2in13.EPD_HEIGHT, 25 ):
            epd.set_frame_memory( image, x, y )
            epd.display_frame()
            epd.set_frame_memory( image, x, y )
            epd.display_frame()
            print( x, y )
    image.close()
    epd.sleep()

if theDisplay == 'B':
    epd = epd2in13b.EPD()
    epd.init()
    epd.Clear()
    epd.sleep()
