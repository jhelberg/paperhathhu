from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer
from watchdog.observers.polling import PollingObserverVFS
from os import listdir
from os import stat
import m3u8

class radioControl( FileSystemEventHandler ):
  def on_any_event( self, event ):
      if event.src_path == "/var/lib/radiod" + "/volume":
          self.changedVolume()
      if event.src_path == "/var/lib/radiod" + "/current_station":
          self.changedStation()

  oldvol = 0
  oldstation= 0

  def getvolume( self ):
     f = open( "/var/lib/radiod" + "/volume", "r" )
     if f.mode == "r":
       try:
         contents = int( f.read() )
       except ValueError:
         f.close()
         return self.oldvol
     f.close()
     self.oldvol = contents
     return contents

  def getservice( self ):
     f = open( "/var/lib/radiod" + "/current_station", "r" )
     if f.mode == "r":
       try:
          contents = int( f.read() )
       except ValueError:
          f.close()
          return self.oldstation
     f.close()
     self.oldstation = contents
     return contents

  def changedVolume( self ):
      print( "V: ", self.getvolume() )

  def changedStation( self ):
      print( "S: ", self.getservice() )

  def __init__(self):
     try:
         self.m3u8_obj = m3u8.load( "/var/lib/mpd/playlists" + "/_Radio.m3u" )
     except IOError:
         inf = "#EXTINF:-1,<No _Radio.m3u>\nhttp://media.example.com/entire.ts\n"
         self.m3u8_obj = m3u8.loads( "#EXTM3U\n" +
                                     inf + inf + inf + inf + inf + inf + inf + inf + inf +
                                     inf + inf + inf + inf + inf + inf + inf + inf + inf +
                                     inf + inf + inf + inf + inf + inf + inf + inf + inf +
                                     inf + inf + inf + inf + inf + inf + inf + inf + inf +
                                     inf + inf + inf + inf + inf + inf + inf + inf + inf +
                                     inf + inf + inf + inf + inf + inf + inf + inf + inf +
                                     inf + inf + inf + inf + inf + inf + inf + inf + inf +
                                     "#EXT-X-ENDLIST" )

  def stationName( self, index ):
      try:
        return self.m3u8_obj.segments[ index - 1 ].title
      except IndexError:
        return "<No name>"

  def startPolling( self ):
     observer = PollingObserverVFS( stat, listdir, polling_interval = 0.2 )
     observer.schedule( self, "/var/lib/radiod" )  # watch the radiod directory
     observer.start()
