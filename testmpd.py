from mpd import MPDClient
from time import sleep
client = MPDClient()
client.timeout = 1
client.idletimeout = None
client.connect( "localhost", 6600 )

while True:
    currentsong = client.currentsong()
    if len( currentsong ) > 0:
        print( str( currentsong.get( "title" ) ) )
    status = client.status()
    print(  str( status.get( "volume" ) ) )
    sleep( 1 )
